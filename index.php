<?php
// require 'animal.php';
require 'frog.php';
require 'ape.php';


$sheep = new Animal("shaun");

echo "Nama domba : ".$sheep->name; // "shaun"
echo "<br>"; 
echo "Jumlah kaki : ".$sheep->legs; // 2
echo "<br>"; 
echo "Berdarah dingin : ".$sheep->cold_blooded; // false

$sungokong = new Ape("kera sakti");

echo "<br><br> Nama Kera : ".$sungokong->name; // "shaun"
echo "<br>"; 
echo "Jumlah kaki : ".$sungokong->legs; // 2
echo "<br>"; 
echo "Berdarah dingin : ".$sungokong->cold_blooded."<br>"; // false
echo "Teriak : ";
$sungokong->yell();

$kodok = new Frog("buduk");

echo "<br><br> Nama Kodok : ".$kodok->name; // "shaun"
echo "<br>"; 
echo "Jumlah kaki : ".$kodok->legs; // 2
echo "<br>"; 
echo "Berdarah dingin : ".$kodok->cold_blooded."<br>"; // false
echo "Lompat : ";
$kodok->jump();


?>